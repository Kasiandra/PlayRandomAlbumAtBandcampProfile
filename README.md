# PlayRandomAlbumAtBandcampProfile

Play a random Album from a bandcamp profile with mpv/youtube-dl

Set vardiable
```
export SOURCELINK="<name>.bandcamp.com"
```

```
$ mpv --shuffle --no-video $(for i in $(curl $SOURCELINK | sed -n 's/.*href="\([^"]*\).*/\1/p'| sed 's/.com.*/.com/g' | grep "/album" |  sort | uniq); do echo $SOURCELINK$i; done)
```